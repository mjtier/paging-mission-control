package tech.michaeltier.pagingmissioncontrol.input;

/**
 * <p>{@code InvalidTelemetryDataInputException} is an <em>unchecked
 * exceptions</em> covering exceptional behavior that occurs as a result of
 * unrecoverable errors experienced during the processing of the satellite telemetry
 * status data. Unchecked exceptions do <em>not</em> need to be
 * declared in a method or constructor's {@code throws} clause if they
 * can be thrown by the execution of the method or constructor and
 * propagate outside the method or constructor boundary.
 */
public class InvalidTelemetryDataInputException extends RuntimeException {

    public InvalidTelemetryDataInputException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@link InvalidTelemetryDataInputException} with the specified detail message and
     * cause.  <p>Note that the detail message associated with {@code cause} is <i>not</i> automatically
     * incorporated in exception's detail message.
     *
     * @param message the detail message
     * @param cause   the cause. A {@code null} value is permitted, and indicates that the cause is nonexistent
     *                or unknown.
     */
    public InvalidTelemetryDataInputException(String message, Throwable cause) {
        super(message, cause);
    }


}
