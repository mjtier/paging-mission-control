package tech.michaeltier.pagingmissioncontrol.input;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Satellite status telemetry data parser. Parses a data representation of the satellite data and produces a
 * {@link tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint} object data model.
 * Each row of the ingest of status telemetry data has the format:
 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 */
@Slf4j
public class StatusTelemetryInputParser {

    /**
     * Number of fields in a single line of well formatted status input data telemetry
     */
    public static final int NUM_FIELDS = 8;
    /**
     * The delimiter string used to delimit the different fields of each row in the text file.
     */
    public static final String FIELD_DELIMITER = "|";
    /**
     * The patern of the timestamp string present in the input file.
     */
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd HH:mm:ss.SSS";


    // Intentionally made private to prevent exposing implicit public constructor
    private StatusTelemetryInputParser() {
    }

    /**
     * Parses a single record of satellite status telemetry data. Where the data schema of a single line is:
     * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
     *
     * @param statusLine a string representation of a single recorded line of the satellite status file
     * @return a single {@link tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint} object data model representation
     * of a single recorded data point.
     */
    public static StatusDataPoint parse(String statusLine) {
        checkNotNull(statusLine, "Null input not accepted for parsing.");
        checkArgument(!statusLine.isEmpty(), "Can't parse a blank line of status data.");
        List<String> tokens = process(statusLine);
        // strict enforcement that the line must contain all 8 fields, no more, no less
        if (tokens.size() != NUM_FIELDS) {
            log.error("Error tokenizing input line >>>{}<<<, expected {} tokens, but receieved {}",
                    statusLine,
                    NUM_FIELDS);
            throw new InvalidTelemetryDataInputException("Exception occurred in tokenizing line of file");
        }

        log.debug("processed the following tokens from the status line: {}", Joiner.on(",").join(tokens));
        DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT);
        // Project statement says we can rely on the input file data being valid, so that means we can count
        // on each line following the specified schema.
        return StatusDataPoint.builder()
                .timestamp(LocalDateTime.parse(tokens.get(0), timestampFormatter))
                .satelliteId(Integer.parseInt(tokens.get(1)))
                .redHighLimit(Double.parseDouble(tokens.get(2)))
                .yellowHighLimit(Double.parseDouble(tokens.get(3)))
                .yellowLowLimit(Double.parseDouble(tokens.get(4)))
                .redLowLimit(Double.parseDouble(tokens.get(5)))
                .rawValue(Double.parseDouble(tokens.get(6)))
                .component(tokens.get(7))
                .build();
    }

    /**
     * Helper function for tokenizing a delimited string. Processes one line of the status data file represted as text
     * and returns a {@link List} of tokenized strings representing the text value of one of the fields of the data
     * file.
     */
    private static List<String> process(String line) {
        if (line.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> results = Splitter.on(FIELD_DELIMITER).trimResults().splitToList(line);
        if (results.size() != NUM_FIELDS) {
            log.error("Input line {} exceeds the maximum of {} fields ", line, NUM_FIELDS);
            throw new InvalidTelemetryDataInputException("Exception occurred in tokenizing line of file");
        }
        return results;

    }
}
