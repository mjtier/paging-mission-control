package tech.michaeltier.pagingmissioncontrol.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Model of a single alert message for a violation of the satellite operating parameters.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Alert {
    private Integer satelliteId;
    private String severity;
    private String component;
    private LocalDateTime timestamp;

}
