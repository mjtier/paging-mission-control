package tech.michaeltier.pagingmissioncontrol.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


/**
 * Model of a single data point in the satellite status telemetry data
 */
@Data
// Causes Lombok to implement the Builder design pattern for the Pojo class.
@Builder
// Causes Lombok to generate a constructor with no parameters.
@NoArgsConstructor
@AllArgsConstructor
public class StatusDataPoint {
    private LocalDateTime timestamp;
    // The unique ID of the satellite
    private Integer satelliteId;
    private Double redHighLimit;
    private Double yellowHighLimit;
    private Double yellowLowLimit;
    private Double redLowLimit;
    private Double rawValue;
    private String component;
}

