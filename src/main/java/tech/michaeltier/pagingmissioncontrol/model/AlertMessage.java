package tech.michaeltier.pagingmissioncontrol.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Data model for output message of alerts
 */
@Data
@NoArgsConstructor
public class AlertMessage {
    private List<Alert> alerts;
}
