package tech.michaeltier.pagingmissioncontrol.model;

public final class ModelConstants {
    public static final String BATTERY_COMPONENT = "BATT";
    public static final String THERMOSTAT_COMPONENT = "TSTAT";
    public static final String RED_HIGH_SEV = "RED HIGH";
    public static final String RED_LOW_SEV = "RED LOW";
    public static int NUMBER_OF_REPEATED_ALARMS_TO_TRIGGER = 3;
}
