package tech.michaeltier.pagingmissioncontrol.persistence;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataRepositoryFactory {
    public static StatusDataRepository create(DataRepositoryType type) {
        Preconditions.checkNotNull(type, "Type of repository to construct can't be null");
        if (type == DataRepositoryType.SIMPLE_IN_MEMORY) {
            return new SimpleDataRepository();
        } else {
            log.error("Attempt to create a factory of type {} when that type is not known.", type.name());
            throw new IllegalStateException("Unknown factory type requested");
        }
    }
}
