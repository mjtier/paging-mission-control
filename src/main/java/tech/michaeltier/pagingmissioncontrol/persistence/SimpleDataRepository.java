package tech.michaeltier.pagingmissioncontrol.persistence;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Slf4j
@NoArgsConstructor
public class SimpleDataRepository implements StatusDataRepository {

    /**
     * Multimap for persisting the serialized satellite telemetry status data in memory.
     */
    private final Map<Integer, List<StatusDataPoint>> dataPointsSatIdMap = new HashMap<>();


    @Override
    public List<StatusDataPoint> findStatusDataBySatelliteId(Integer satelliteId) {
        Preconditions.checkNotNull(satelliteId, "Must provide a non-null value for satelliteId.");
        Preconditions.checkArgument(satelliteId > 0,
                "Must provide a valid satellite ID to search for");
        Preconditions.checkArgument(dataPointsSatIdMap.containsKey(satelliteId),
                "Must provide a satellite ID that is stored in the repository.");

        return dataPointsSatIdMap.get(satelliteId);
    }


    @Override
    public List<StatusDataPoint> findStatusDataBySatelliteIdAndComponent(Integer satelliteId, String component) {
        Preconditions.checkNotNull(satelliteId, "Must provide a non-null value for satelliteId.");
        Preconditions.checkArgument(satelliteId > 0,
                "Must provide a valid satellite ID to search for");
        Preconditions.checkArgument(dataPointsSatIdMap.containsKey(satelliteId),
                "Must provide a satellite ID that is stored in the repository.");
        Preconditions.checkNotNull(component, "Must provide a non-null value for componenet.");
        Preconditions.checkArgument(!component.isEmpty(),
                "Must provide a valid non-empty componenet name to search on");

        var dataPointsForSat = dataPointsSatIdMap.get(satelliteId);
        Predicate<StatusDataPoint> byComponenet = dp -> dp.getComponent().equals(component);
        var result = dataPointsForSat.stream().filter(byComponenet).collect(Collectors.toList());

        return result;
    }


    @Override
    public Set<Integer> findAllSatelliteIds() {
        return dataPointsSatIdMap.keySet();
    }


    @Override
    public void store(StatusDataPoint datapoint) {
        Preconditions.checkNotNull(datapoint, "Null value for data point not allowed");
        log.debug("Storing data point {}", datapoint);
        Integer satId = datapoint.getSatelliteId();
        if (dataPointsSatIdMap.containsKey(datapoint.getSatelliteId())) {
            // The satellite ID key exists, so a list of data points should already have been created. That means
            // we can just append the new entry to the existing list of data points.
            dataPointsSatIdMap.get(datapoint.getSatelliteId()).add(datapoint);
        } else {
            // Create and initialize new list
            var newList = new ArrayList<StatusDataPoint>() {{
                add(datapoint);
            }};
            dataPointsSatIdMap.put(datapoint.getSatelliteId(), newList);
        }

    }

    @Override
    public Integer count() {
        var listOfDataPoints = dataPointsSatIdMap.values();
        int count = 0;
        for (var list : listOfDataPoints) {
            count = count + list.size();
        }
        return count;
    }


}
