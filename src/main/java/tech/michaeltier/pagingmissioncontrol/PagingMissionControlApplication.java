package tech.michaeltier.pagingmissioncontrol;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.Alert;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;
import tech.michaeltier.pagingmissioncontrol.monitor.Monitor;
import tech.michaeltier.pagingmissioncontrol.service.AlertService;
import tech.michaeltier.pagingmissioncontrol.service.StatusDataService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 */
@Slf4j
public class PagingMissionControlApplication {

    public static void main(String[] args) {
        log.info("Running application to process satellite status data...");
        int numberOfArgs = args.length;
        if (numberOfArgs != 1) {
            log.error("The application was passed {} arguments on startup.", numberOfArgs);
        }

        String filePath = args[0];
        // Create out persistence layer for use by the services
        final StatusDataService dataService = new StatusDataService();
        dataService.ingestFile(filePath);
        Set<Integer> idSet = dataService.getAllSatelliteIds();
        final Integer[] ids = dataService.getAllSatelliteIds().toArray(new Integer[idSet.size()]);

        // Start data retrieval and analysis
        final var dataPntsFilteredBySat1AndBattComp
                = dataService.getDataBySatelliteIdAndComponent(ids[0], ModelConstants.BATTERY_COMPONENT);

        final var sat1DataPointsInBattAlarm
                = Monitor.getDataPointsInVoltageAlarm(dataPntsFilteredBySat1AndBattComp);


        List<Alert> sat1Alerts = AlertService.analyzBatteryAlarms(sat1DataPointsInBattAlarm);
        List<Alert> cummulativeAlerts = new ArrayList<>(sat1Alerts);

        final var dataPntsFilteredBySat2AndBattComp
                = dataService.getDataBySatelliteIdAndComponent(ids[1], ModelConstants.BATTERY_COMPONENT);
        final var sat2DataPointsInBattAlarm
                = Monitor.getDataPointsInVoltageAlarm(dataPntsFilteredBySat2AndBattComp);
        List<Alert> sat2Alerts = AlertService.analyzBatteryAlarms(sat2DataPointsInBattAlarm);
        cummulativeAlerts.addAll(sat2Alerts);

        // Now handle the Temperature alerts
        final var dataPntsFilteredBySat1AndThermComp = dataService.getDataBySatelliteIdAndComponent(ids[0], ModelConstants.THERMOSTAT_COMPONENT);
        final var dataPointsSat1InTempAlarm = Monitor.getDataPointsInTemperatureAlarm(dataPntsFilteredBySat1AndThermComp);
        List<Alert> sat1TempAlerts = AlertService.analyzeTempAlarms(dataPointsSat1InTempAlarm);
        cummulativeAlerts.addAll(sat1TempAlerts);

        final var dataPntsFilteredBySat2AndThermComp = dataService.getDataBySatelliteIdAndComponent(ids[1], ModelConstants.THERMOSTAT_COMPONENT);
        final var dataPointsSat2InTempAlarm = Monitor.getDataPointsInTemperatureAlarm(dataPntsFilteredBySat2AndThermComp);
        List<Alert> sat2TempAlerts = AlertService.analyzeTempAlarms(dataPointsSat2InTempAlarm);
        cummulativeAlerts.addAll(sat2TempAlerts);


        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
        String jsonCollection =  gson.toJson(cummulativeAlerts);
        log.info(jsonCollection);
        System.exit(0);
    }
}
