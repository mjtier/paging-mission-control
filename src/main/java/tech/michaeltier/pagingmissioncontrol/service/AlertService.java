package tech.michaeltier.pagingmissioncontrol.service;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.Alert;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;
import tech.michaeltier.pagingmissioncontrol.monitor.DataPointSlidingWindow;
import tech.michaeltier.pagingmissioncontrol.monitor.Monitor;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@NoArgsConstructor
public class AlertService {

    public static final int ALERT_DURATION_WINDOW = 5;

    public static List<Alert> analyzeTempAlarms(List<StatusDataPoint> dataPoints) {
        var points = Monitor.getDataPointsInTemperatureAlarm(dataPoints);
        DataPointSlidingWindow window = new DataPointSlidingWindow(ALERT_DURATION_WINDOW);
        List<Alert> result = new ArrayList<>();
        for(int i=0; i < dataPoints.size(); i++) {
            if(dataPoints.size() - i < ModelConstants.NUMBER_OF_REPEATED_ALARMS_TO_TRIGGER ) {
                break;
            }
            Alert alert = window.getAlertWithinWindows(dataPoints,dataPoints.get(i).getTimestamp(), i);
            result.add(alert);
        }

        return result;
    }

    public static List<Alert> analyzBatteryAlarms(List<StatusDataPoint> dataPoints) {
        var points = Monitor.getDataPointsInVoltageAlarm(dataPoints);
        DataPointSlidingWindow window = new DataPointSlidingWindow(ALERT_DURATION_WINDOW);
        List<Alert> result = new ArrayList<>();
        for(int i=0; i < dataPoints.size(); i++) {
            if(dataPoints.size() - i < ModelConstants.NUMBER_OF_REPEATED_ALARMS_TO_TRIGGER ) {
                break;
            }
            Alert alert = window.getAlertWithinWindows(dataPoints,dataPoints.get(i).getTimestamp(), i);
            result.add(alert);
        }

        return result;
    }

}
