package tech.michaeltier.pagingmissioncontrol.monitor;

import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class Monitor {

    /**
     * Returns all {@link ModelConstants#THERMOSTAT_COMPONENT} components data points that are in alarm
     *
     * @param A list of data points from the thermostat component
     * @return A list of the data points where the thermostat reading exceeds the red high limit
     */
    public static List<StatusDataPoint> getDataPointsInTemperatureAlarm(List<StatusDataPoint> dataPoints) {
        Predicate<StatusDataPoint> inTempAlarm = dp -> dp.getRawValue() > dp.getRedHighLimit();
        var result = dataPoints.stream().filter(inTempAlarm).collect(Collectors.toList());
        return result;
    }

    /**
     * Returns all {@link ModelConstants#BATTERY_COMPONENT} components data points that are in alarm
     *
     * @param A list of data points from the battery component
     * @return A list of the data points where the battery voltage readings are under the red low limit
     */
    public static List<StatusDataPoint> getDataPointsInVoltageAlarm(List<StatusDataPoint> dataPoints) {
        Predicate<StatusDataPoint> inBatteryAlarm = dp -> dp.getRawValue() < dp.getRedLowLimit();
        var result = dataPoints.stream().filter(inBatteryAlarm).collect(Collectors.toList());
        return result;
    }
}



