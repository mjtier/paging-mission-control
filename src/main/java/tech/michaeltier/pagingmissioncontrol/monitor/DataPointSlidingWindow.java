package tech.michaeltier.pagingmissioncontrol.monitor;

import lombok.extern.slf4j.Slf4j;
import tech.michaeltier.pagingmissioncontrol.model.Alert;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Given a configured duration and event count, keep track of times within an amount of time relative to that interval.
 *
 * If needed we could record a list appropriate pieces of the telemetry data instead of just the time.
 */
@Slf4j
public class DataPointSlidingWindow {
    private long duration;

    /**
     *
     * @param duration  time of width of window in minutes
     */
    public DataPointSlidingWindow(long duration) {
        this.duration = duration;
    }


    public Alert getAlertWithinWindows(List<StatusDataPoint> dataPoints, LocalDateTime rightWindowTime, int rightWindowIdx) {
        var pointsInWindow = getPointsInDuration(dataPoints, rightWindowTime.plusMinutes(duration));
        if(pointsInWindow.size() < ModelConstants.NUMBER_OF_REPEATED_ALARMS_TO_TRIGGER) {
            return null;
        }

        String component = pointsInWindow.get(0).getComponent();
        String severity;
        if(component.equals(ModelConstants.THERMOSTAT_COMPONENT)) {
            severity = ModelConstants.RED_HIGH_SEV;
        }
        else {
            severity = ModelConstants.RED_LOW_SEV;
        }

        return Alert.builder()
        .component(component)
        .satelliteId(pointsInWindow.get(0).getSatelliteId())
        .severity(severity)
        .timestamp(pointsInWindow.get(0).getTimestamp()).build();

    }


    private List<StatusDataPoint> getPointsInDuration(List<StatusDataPoint> points, LocalDateTime end) {
        Predicate<StatusDataPoint> isWithinRange = dp -> dp.getTimestamp().isBefore(end);
        return points.stream().filter(isWithinRange).collect(Collectors.toList());
    }

}
