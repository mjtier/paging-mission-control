package tech.michaeltier.pagingmissioncontrol.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tech.michaeltier.pagingmissioncontrol.model.Alert;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;
import tech.michaeltier.pagingmissioncontrol.monitor.Monitor;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

class AlertServiceTest {

    private  List<String> listOfRawDataLines;

    @BeforeEach
    void setUp() {
        listOfRawDataLines = List.of(
                "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT",
                "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT",
                "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT",
                "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT",
                "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT",
                "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT",
                "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT",
                "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT",
                "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT",
                "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT",
                "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT",
                "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT",
                "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT",
                "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT");

    }

    @AfterEach
    void tearDown() {
    }

    @Tag("analysis")
    @Test
    void TestAlarmService_TempAlarmAlertGeneration_ShouldPass() {
        int expectedNumberOfDataPoints = listOfRawDataLines.size();
        StatusDataService dataService = new StatusDataService();
        for(var rawDataPoint: listOfRawDataLines) {
            dataService.ingestLine(rawDataPoint);
        }
        assertThat(expectedNumberOfDataPoints).isEqualTo(dataService.getDataPointCount());
        var dataPntsFilteredByIdAndBattComp = dataService.getDataBySatelliteIdAndComponent(1000, ModelConstants.BATTERY_COMPONENT);
        assertThat(3).isEqualTo(Monitor.getDataPointsInVoltageAlarm(dataPntsFilteredByIdAndBattComp).size());
        dataPntsFilteredByIdAndBattComp = dataService.getDataBySatelliteIdAndComponent(1001, ModelConstants.BATTERY_COMPONENT);
        assertThat(1).isEqualTo(Monitor.getDataPointsInVoltageAlarm(dataPntsFilteredByIdAndBattComp).size());

        var dataPntsFilteredByIdAndThermComp = dataService.getDataBySatelliteIdAndComponent(1000, ModelConstants.THERMOSTAT_COMPONENT);
        var dataPointsInTempAlarm = Monitor.getDataPointsInTemperatureAlarm(dataPntsFilteredByIdAndThermComp);

        List<Alert> alerts = AlertService.analyzeTempAlarms(dataPointsInTempAlarm);
        assertThat(alerts.size()).isEqualTo(1);
    }

    void TestAlarmService_VoltageAlarmAlertGeneration_ShouldPass() {
        int expectedNumberOfDataPoints = listOfRawDataLines.size();
        StatusDataService dataService = new StatusDataService();
        for(var rawDataPoint: listOfRawDataLines) {
            dataService.ingestLine(rawDataPoint);
        }
        assertThat(expectedNumberOfDataPoints).isEqualTo(dataService.getDataPointCount());
        var dataPntsFilteredByIdAndBattComp = dataService.getDataBySatelliteIdAndComponent(1000, ModelConstants.BATTERY_COMPONENT);
        var dataPointsInBattAlarm = Monitor.getDataPointsInVoltageAlarm(dataPntsFilteredByIdAndBattComp);
        assertThat(dataPointsInBattAlarm.size()).isEqualTo(3);

        List<Alert> alerts = AlertService.analyzBatteryAlarms(dataPointsInBattAlarm);
        assertThat(alerts.size()).isEqualTo(1);
        assertThat(alerts.get(0).getTimestamp()).isEqualTo(dataPointsInBattAlarm.get(0).getTimestamp());
        assertThat(alerts.get(0).getSeverity()).isEqualTo(ModelConstants.RED_LOW_SEV);
    }
}