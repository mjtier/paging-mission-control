package tech.michaeltier.pagingmissioncontrol.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tech.michaeltier.pagingmissioncontrol.model.ModelConstants;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;


class StatusDataServiceTest {
    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Tag("input")
    @Test
    void TestIngest_ValidStreamInput_SingleDatatPoint_ShouldPass() {
        String rawData = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
        int expectedNumberOfDataPoints = 1;
        int expectedNumberOfSatellites = 1;
        StatusDataService dataService = new StatusDataService();
        dataService.ingestLine(rawData);
        assertThat(dataService.getDataPointCount()).isEqualTo(expectedNumberOfDataPoints);
        assertThat(dataService.getAllSatelliteIds().size()).isEqualTo(expectedNumberOfSatellites);
        assertThat(1001).isIn(dataService.getAllSatelliteIds());

    }

    @Tag("input")
    @Test
    void TestIngest_ValidStreamInput_MultipleDatatPoints_ShouldPass() {

        List<String> listOfRawDataLines = List.of(
                "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT",
                "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT",
                "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT",
                "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT",
                "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT",
                "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT",
                "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT",
                "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT",
                "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT",
                "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT",
                "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT",
                "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT",
                "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT",
                "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT");

        int expectedNumberOfDataPoints = listOfRawDataLines.size();
        StatusDataService dataService = new StatusDataService();
        for(var rawDataPoint: listOfRawDataLines) {
            dataService.ingestLine(rawDataPoint);
        }
        assertThat(expectedNumberOfDataPoints).isEqualTo(dataService.getDataPointCount());

        assertThat(2).isEqualTo(dataService.getAllSatelliteIds().size());
        assertThat(1001).isIn(dataService.getAllSatelliteIds());
        assertThat(1000).isIn(dataService.getAllSatelliteIds());
        assertThat(7).isEqualTo(dataService.getDataBySatelliteId(1000).size());
        assertThat(3).isEqualTo(dataService.getDataBySatelliteIdAndComponent(1000, ModelConstants.BATTERY_COMPONENT).size());
        assertThat(dataService.getDataBySatelliteIdAndComponent(1000, ModelConstants.THERMOSTAT_COMPONENT).size()).isEqualTo(4);
    }
}