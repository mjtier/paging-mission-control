package tech.michaeltier.pagingmissioncontrol.input;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tech.michaeltier.pagingmissioncontrol.model.StatusDataPoint;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatusTelemetryInputParserTest {

    @Tag("input")
    @Test
    void TestParse_ValidInputStatusLine_ShouldPass() {
        DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern(StatusTelemetryInputParser.TIMESTAMP_FORMAT);
        StatusDataPoint expectedStatusDataPointFromParse = StatusDataPoint.builder()
                .timestamp(LocalDateTime.parse("20180101 23:01:05.001", timestampFormatter))
                .satelliteId(Integer.parseInt("1001"))
                .redHighLimit(Double.parseDouble("101"))
                .yellowHighLimit(Double.parseDouble("98"))
                .yellowLowLimit(Double.parseDouble("25"))
                .redLowLimit(Double.parseDouble("20"))
                .rawValue(Double.parseDouble("99.9"))
                .component("TSTAT")
                .build();
        String statusLine = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
        StatusDataPoint actualStatusDataPoint = StatusTelemetryInputParser.parse(statusLine);
        assertEquals(expectedStatusDataPointFromParse, actualStatusDataPoint);
        assertEquals(99.9, actualStatusDataPoint.getRawValue());

    }

    @Tag("input")
    @Test
    void TestParse_BlankLine_ShouldThrowException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            StatusTelemetryInputParser.parse("");
        });

    }

    @Tag("input")
    @Test
    void TestParse_TooManyFields_ShouldThrowException() {
        Assertions.assertThrows(InvalidTelemetryDataInputException.class, () -> {
            StatusTelemetryInputParser.parse("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT|NOT_VALID");
        });

    }


}