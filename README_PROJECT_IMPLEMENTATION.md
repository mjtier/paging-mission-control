# Project Implementation Information

## Assumptions 
* There are only two satellites sending status data that is being recorded in the file. The implemeneation would accomidate the expansion in the number of satellites up until 2^31 individual satellites.
* The problem statement doesn't say that the pair of satellites are always identified as 1000 and 1001 as shown in the sample data set, so an assumption was made that a satellite's ID will always be a value
that could fit within a Java primitive int.
## Design & Architecture
### Input File Parsing
Use of Streams introduced in Java 8 to parse file as holding entire file in memory could crash the JVM
for very large files

## Data Model

### StatusDataPoint
This class is the model of a single data point in the satellite status telemetry data.

Because of the assumption so these values were not used as constants for  a satellite's unique ID. Therefore, it was decided that number that will fit into a Java int  



## Setup 
## Dependencies

* Java 11 JDK
* Gradle 6.7

## Building The Exectuable
The assembled .jar is stored in {Project Root}/build/libs directory.
You can build the jar and run all the associated unit test by issuing the
`./gradelw assemble` command in the command line interface.


## Logging
The project logs to both the console and to a file. The logging file is placed in a folder 

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)